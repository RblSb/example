package;

import kha.Framebuffer;
import kha.Scheduler;
import kha.Color;
import kha.Image;
import kha.Assets;
import kha.graphics2.Graphics;
import kha.input.KeyCode;
import kha.input.Keyboard;
import kha.math.FastMatrix3;
import kha.FastFloat;
import kha.System;
using utils.MathExtension;
using kha.graphics2.GraphicsExtension;

class Ship {

	var count = 0;
	var x:Float;
	var y:Float;
	var angle:Float;
	var angleSpeed:Float;
	var speed:Float;
	var angleT:Float;
	var keys:Map<KeyCode, Bool>;

	public function new(x:Float, y:Float, angle:Float, angleSpeed:Float, keys:Map<KeyCode, Bool>) {
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.angleSpeed = angleSpeed;
		this.keys = keys;
		speed = 0;
		angleT = speed;
	}

	public function update():Void {
		if (keys[Up]) {
			speed += 0.1;
		}
		if (keys[Down]) {
			speed -= 0.1;
			if (speed < 0) speed = 0;
		}

		if (keys[Right]) {
			angle += angleSpeed;
			angleT -= 0.02;
			if (angleT < 0.3) angleT = 0.3;

		} else if (keys[Left]) {
			angle -= angleSpeed;
			angleT -= 0.02;
			if (angleT < 0.3) angleT = 0.3;

		} else angleT += 0.05;

		if (angleT > speed) angleT = speed;
		if (angle < 0) angle = 360;
		if (angle > 360) angle = 0;
		x += Math.cos(angle.toRad()) * angleT;
		y += Math.sin(angle.toRad()) * angleT;
	}

	var tempMatrix = FastMatrix3.identity();

	public function render(g:Graphics):Void {
		tempMatrix.setFrom(g.transformation);
		g.transformation = g.transformation.multmat(
			rotation(angle.toRad(), x + 24, y + 20)
		);
		g.drawImage(Project.img, x, y);
		g.transformation = tempMatrix;
		g.drawString("Angle: " + angle, 10, 10);
	}

	inline function rotation(angle: FastFloat, centerx: FastFloat, centery: FastFloat): FastMatrix3 {
		return FastMatrix3.translation(centerx, centery)
			.multmat(FastMatrix3.rotation(angle))
			.multmat(FastMatrix3.translation(-centerx, -centery));
	}

}
