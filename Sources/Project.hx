package;

import kha.Framebuffer;
import kha.Scheduler;
import kha.graphics2.Graphics;
using kha.graphics2.GraphicsExtension;
import kha.Image;
import kha.Font;
import kha.Assets;
import kha.input.KeyCode;
import kha.input.Keyboard;
import kha.System;
import kha.Canvas;

class Project extends Screen {

	public static var img:Image;
	public static var ships:Array<Ship> = [];

	public function init() {
		for (i in 0...1) {
			ships.push(
				new Ship(
					Std.random(Std.int(Screen.w / 2)),
					Std.random(Std.int(Screen.h / 2)),
					Std.random(360), 3,
					keys
				)
			);
		}
		img = Assets.images.ship;
	}

	override function onKeyDown(key:KeyCode):Void {
		if (key == 189 || key == KeyCode.HyphenMinus) {
			if (scale > 1) setScale(scale - 0.5);

		} else if (key == 187 || key == KeyCode.Equals) {
			if (scale < 9) setScale(scale + 0.5);
		}
	}

	override function onUpdate():Void {
		for (ship in ships) ship.update();
	}

	override function onRender(canvas:Canvas):Void {
		var g = canvas.g2;
		g.begin(true, kha.Color.Black);
		g.font = Assets.fonts.font;
		g.fontSize = 16;
		//g.drawString('Width: '+Main.w, 10, 10);
		//g.drawString('Height: '+Main.h, 10, 12+g.font.height(g.fontSize));
		for (ship in ships) ship.render(g);
		g.end();
	}

}
